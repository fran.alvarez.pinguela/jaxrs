package com.informaticapinguela.rest.util;

import java.security.Key;

public interface KeyGenerator {
	Key getPrivateKey();
	Key getPublicKey();
}
