package com.informaticapinguela.rest.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import io.jsonwebtoken.SignatureAlgorithm;

public class SimpleKeyGenerator implements KeyGenerator {

	/**
	 * Le a clave privada de /src/main/resources/pvt.key ->despregado a <war>/WEB-INF/classes/pvt.key
	 */
	@Override
	public Key getPrivateKey() {
		PrivateKey privateKey = null;
		File file = new File (getClass().getClassLoader().getResource("pvt.key").getFile());
		java.nio.file.Path path = Paths.get(file.getAbsolutePath());
		
		
		try {
			/* Read   all bytes from the private key file */
			byte[] bytes  = Files.readAllBytes(path);
			
			/* Generate private key. */
			PKCS8EncodedKeySpec ks = new PKCS8EncodedKeySpec(bytes);
			KeyFactory kf = KeyFactory.getInstance("RSA");
			privateKey = kf.generatePrivate(ks);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return privateKey;
	}
	/**
	 * Le a clave publica de /src/main/resources/pub.key ->despregado a <war>/WEB-INF/classes/pub.key
	 */
	@Override
	public Key getPublicKey() {
		File file = new File (getClass().getClassLoader().getResource("pub.key").getFile());
		java.nio.file.Path path = Paths.get(file.getAbsolutePath());
		byte[] bytes;
		PublicKey publicKey=null;
		try {
			bytes = Files.readAllBytes(path);
			/* Generate public key. */
			X509EncodedKeySpec ks = new X509EncodedKeySpec(bytes);
			KeyFactory kf = KeyFactory.getInstance("RSA");
			publicKey = kf.generatePublic(ks);
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return publicKey;
	}
	
	/*
	// codigo de xeración das claves publica e privada e copia aos ficheiros:
	 public static void main(String[] args) {
		
		KeyPairGenerator kpg;
		try {
			kpg = KeyPairGenerator.getInstance("RSA");
			kpg.initialize(2048);
			KeyPair kp = kpg.generateKeyPair();
			Key pub = kp.getPublic();
			Key pvt = kp.getPrivate();
			System.out.println(pvt.getFormat());
			String pvtoutFile = "z:/pvt";
			String puboutFile = "z:/pub";
			FileOutputStream pvtout;
			FileOutputStream pubout;
			try {
				pvtout = new FileOutputStream(pvtoutFile + ".key");
				pvtout.write(pvt.getEncoded());
				System.out.println(Base64.getEncoder().encodeToString(pvt.getEncoded()));
				pvtout.close();
				pubout = new FileOutputStream(puboutFile + ".pub");
				pubout.write(pub.getEncoded());
				pubout.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}	
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}
	*/
	 

}
