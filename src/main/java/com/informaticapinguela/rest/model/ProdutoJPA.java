package com.informaticapinguela.rest.model;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Stateless
public class ProdutoJPA {

	@PersistenceContext
	private EntityManager entityManager;

	public List<Produto> getAll() {
		return entityManager.createQuery("FROM Produto u", Produto.class).getResultList();
	}

	public Produto getByCodigo(String codigo) {
		return entityManager.find(Produto.class, codigo);
	}
	//@Transactional
	public void create(Produto produto) throws Exception {
		// As excepcións que poidan ocurrir (produto duplicado....) non se poden capturar aquí porque
		// non se lanzan ata rematar a transacción. Hay que capturalas no método chamante.
		
			System.out.println("persisto produto: "+ produto);
			entityManager.persist(produto);
	}
	//@Transactional
	public void update(Produto produto) throws Exception {
		// As excepcións que poidan ocurrir (produto duplicado....) non se poden capturar aquí porque
		// non se lanzan ata rematar a transacción. Hay que capturalas no método chamante.
		
		    System.out.println("actualizo produto: "+ produto);
			entityManager.merge(produto);
	}

	public boolean delete(String codigo) {
		boolean exito = false;
		try {
			Produto produtoBD = entityManager.find(Produto.class, codigo);
			if (produtoBD != null) {
				entityManager.remove(produtoBD);
				exito = true;
			}
		} catch (Exception e) {
		} finally {
		}
		return exito;
	}

}
