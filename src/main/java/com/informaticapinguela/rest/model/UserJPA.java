package com.informaticapinguela.rest.model;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.NotFoundException;



@Stateless
public class UserJPA {
	
	
@PersistenceContext
private EntityManager entityManager;

	
    public List < User > findAll() {
		return entityManager
	            .createQuery("FROM User u", User.class)
	            .getResultList(); 
    }

    public User getById(long id)  {
    	return entityManager.find(User.class, id);
    }
    
    public User getByEmail(String email)  {
    	User user = null;
    	Query query = entityManager.createQuery("SELECT u FROM User u"
    			+ " WHERE u.email=:email");
    	query.setParameter("email", email);
    	
    	try {
    		user = (User) query.getSingleResult();	
    	}catch (NoResultException n) {
    		user=null;
    	}
    	return user;
    }

    public  void create(User usuario) {
    	entityManager.persist(usuario);
    }
    
    public  boolean update(User user) {
    	boolean exito=false;
    	User userBD;
    	// si existe en BD usuario co mesmo email e ten o mesmo id
    	if ((userBD = getByEmail(user.getEmail())) != null  && userBD.getId().equals(user.getId())) {
    		if (user.getName()!= null ) {
        		userBD.setName(user.getName());
    		}
    		if (user.getPassword()!= null) {
    			userBD.setPassword(user.getPassword());
    		}
    		entityManager.merge(userBD);
    		exito=true;
    	}
    	return exito;

    }

    public  void delete(Long id)  {    	
    	User userBD= entityManager.find(User.class, id);
    	if(userBD != null) {
    		entityManager.remove(userBD);
    	}
    }
    
    public boolean autentica(String email, String password) {
		try {
			User userBD = getByEmail(email);
			if (userBD != null && userBD.getPassword().equals(password)) {
				return true;
			}
		} catch (Exception e) {

		}
		return false;
    }
}
