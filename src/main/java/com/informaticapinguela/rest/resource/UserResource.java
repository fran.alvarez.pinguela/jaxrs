package com.informaticapinguela.rest.resource;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.informaticapinguela.rest.model.User;
import com.informaticapinguela.rest.model.UserJPA;

/**
 * CRUD Rest APIs for User Resource
 *
 **/

@Path("users")
public class UserResource {
	
	@Inject
	private UserJPA userJPA;



    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers() {

        List < User > users = userJPA.findAll();

        if (!users.isEmpty()) {
            return Response.ok(users).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @Path("/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserById(@PathParam("id") Long id) {
    	//System.out.println("busco user con id: "+ id);
        User user = userJPA.getById(id);
        //System.out.println("encuentro user: "+user);
        
        //if (user.getId() != null) {
        if (user != null) {
            return Response.ok(user).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createUser(User user) {
    	try {
    		userJPA.create(user);
    		return Response.ok().status(Response.Status.CREATED).build();
    	}catch (Exception e) {
    		return Response.notModified().build();
    	}
    }


    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateUser(@PathParam("id") long id, User user) {
    	user.setId(id);
    	System.out.println("put recibe user completo: "+user);

        boolean result = userJPA.update(user);

        if (result) {
            return Response.ok().status(Response.Status.NO_CONTENT).build();
        } else {
            return Response.notModified().build();
        }
    }

    @Path("/{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteUser(@PathParam("id") Long id) {
    	
    	try {
    		userJPA.delete(id);
    		return Response.ok().status(Response.Status.NO_CONTENT).build();
    	}catch (Exception e) {
    		return Response.notModified().build();  
        }
    }
}
