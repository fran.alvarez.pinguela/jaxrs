package com.informaticapinguela.rest.resource;

import javax.ejb.EJBException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
/**
 * Captura excepcións de RESTEasy
 * @author fran
 *
 */
@Provider
public class EJBExceptionMapper implements  ExceptionMapper<EJBException> {

	@Override
	public Response toResponse(EJBException exception) {

		return Response.status(500).build();
	}

}
