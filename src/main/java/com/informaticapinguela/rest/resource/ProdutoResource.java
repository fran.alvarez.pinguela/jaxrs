package com.informaticapinguela.rest.resource;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.informaticapinguela.rest.model.Produto;
import com.informaticapinguela.rest.model.ProdutoJPA;

@JWTTokenNeeded
@Path("produtos")
public class ProdutoResource {

	@Inject
	private ProdutoJPA produtoJPA;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers() {

        List < Produto > produtos = produtoJPA.getAll();

        if (!produtos.isEmpty()) {
            return Response.ok(produtos).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @Path("/{codigo}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserById(@PathParam("codigo") String codigo) {
        Produto produto = produtoJPA.getByCodigo(codigo);

        if (produto != null) {
            return Response.ok(produto).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createProduto(Produto produto) {
    	
    	try {
    		produtoJPA.create(produto);
    		return Response.ok().status(Response.Status.CREATED).build();
    	}catch (Exception e) {
    		return Response.notModified().build();
    	}
    }


    @PUT
    @Path("/{codigo}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateUser(@PathParam("codigo") String codigo, Produto produto) {
    	produto.setCodigo(codigo);
    	System.out.println("put recibe produto completo: "+produto);

    	try {
    		produtoJPA.update(produto);
    		return Response.ok().status(Response.Status.NO_CONTENT).build();
    	}catch (Exception e) {
    		return Response.notModified().build();
    	}	
    }


    @Path("/{codigo}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteUser(@PathParam("codigo") String codigo) {
        boolean result = produtoJPA.delete(codigo);

        if (result) {
            return Response.ok().status(Response.Status.NO_CONTENT).build();
        } else {
            return Response.notModified().build();
        }
    }
	
	
}
