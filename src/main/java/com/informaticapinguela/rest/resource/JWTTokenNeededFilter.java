package com.informaticapinguela.rest.resource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Key;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;

import javax.annotation.Priority;

import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import com.informaticapinguela.rest.util.KeyGenerator;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;



@JWTTokenNeeded
@Provider
@Priority(Priorities.AUTHENTICATION)
public class JWTTokenNeededFilter implements ContainerRequestFilter {
	@Inject
	private KeyGenerator keyGenerator;
	
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		
		try {
			// Get the HTTP Authorization header from the request
			String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
		
			// Extract the token from the HTTP Authorization header
			// lanza exception si no trae cabecera Authorization Bearer e envía response unauthorized
			String token = authorizationHeader.substring("Bearer".length()).trim();
	
			// validate the token (RSA)	
			// lanza excepción de non vai ben e envía response unauthorized
			Key publicKey = keyGenerator.getPublicKey();
			Jws<Claims> jwt = Jwts.parserBuilder()
					.setSigningKey(publicKey)
					.build()
					.parseClaimsJws(token);		
			System.out.println(jwt);
		    
		} catch (Exception e) {
			e.printStackTrace();
			requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
		}

	}

}
