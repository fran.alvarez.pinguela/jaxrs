package com.informaticapinguela.rest.resource;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.Provider.Service;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.TreeSet;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.informaticapinguela.rest.model.User;
import com.informaticapinguela.rest.model.UserJPA;
import com.informaticapinguela.rest.util.KeyGenerator;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Path("api")
@Transactional
public class AccesoResource {
	@Inject
	private UserJPA userJPA;
	
	@Inject
	private  KeyGenerator keyGenerator;
	
	
	@Context
	private UriInfo uriInfo;
	
	@Path("auth")
	@POST 
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response authUser(User user) {
		// enviase user como json só cos campos de email e password
		boolean authCorrecta;
		String token = null;
		try {
			authCorrecta = userJPA.autentica(user.getEmail(), user.getPassword());
			try {
				token = issueToken(user.getEmail());
				System.out.println("token xerado: " + token);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}

		} catch (Exception e) {
			authCorrecta = false;
		}
		if (authCorrecta) {
			return Response.ok().header("Access-Control-Allow-Origin", "*").header("Authorization", "Bearer " + token)
					.build();
		} else {
			JsonObject json = Json.createObjectBuilder().add("mensaxe", "credenciais incorrectas").build();
			return Response.status(Response.Status.UNAUTHORIZED).entity(json).build();
		}
	}
	private String issueToken(String email) {
		
		Key privateKey = keyGenerator.getPrivateKey();
		
		String jwtToken = Jwts.builder()
				.setSubject(email)
				.setIssuer(uriInfo.getAbsolutePath().toString())
				.setIssuedAt(new Date())
				.setExpiration(Timestamp.valueOf(LocalDateTime.now().plusMinutes(2L)))
				.signWith(privateKey)
				.compact();
		return jwtToken; 	   	 	
	}
	
	@Path("register")
	@POST 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
	public Response registerUser(User user) {
		
		System.out.println("tento rexistrar user: "+user.getEmail());
		try {
			userJPA.create(user);
			return Response.ok().status(Response.Status.CREATED).build();
		}catch (Exception e) {
			System.out.println(e);
		}
		 return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		
	}
	

}
