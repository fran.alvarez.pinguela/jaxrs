import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import com.informaticapinguela.rest.model.User;

public class ClientUserTest {
	private static final String FULL_PATH = "http://localhost:8081/jaxrs/rest/users";

	public static void main(String[] args) {
		System.out.println("testListAllUsers(): ");
		testListAllUsers();
		System.out.println("testGetUser() : ");
		testGetUser() ;
		System.out.println("testCreateUser(): ");
		testCreateUser();
		System.out.println("testUpdateUser(): ");
		testUpdateUser();
		System.out.println("testDeleteUser(): ");
		testDeleteUser();
	}

	public static void testListAllUsers() {
		final ResteasyClient client = new ResteasyClientBuilder().build();
		final ResteasyWebTarget target = client.target(FULL_PATH);
		String response = target.request().get(String.class);
		System.out.println(response);
	}

	public static void testGetUser() {

		final ResteasyClient client = new ResteasyClientBuilder().build();
		final ResteasyWebTarget target = client.target(FULL_PATH + "/100");
		Response response = target.request().get();
		User user = response.readEntity(User.class);
		System.out.println(user.toString());
		response.close();
	}

	public static  void testCreateUser() {
	        ResteasyClient client = new ResteasyClientBuilder().build();
	        ResteasyWebTarget target = client.target(FULL_PATH);
	        Response response = target.request()
	            .post(Entity.entity(new User(103L, "Amir", "amir@gmail.com"), "application/json"));
	        System.out.println(response.getStatus());
	        response.close();
	    }

	public static void testUpdateUser() {
		User user = new User();
		user.setId(100L);
		user.setName("Ram");
		user.setEmail("ram@gmail.com");
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(FULL_PATH + "/100");
		Response response = target.request().put(Entity.entity(user, "application/json"));
		System.out.println(response.getStatus());
		response.close();
	}
	public static void testDeleteUser() {
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(FULL_PATH + "/101");
		Response response = target.request().delete();
		System.out.println(response.getStatus());
		response.close();

		final ResteasyWebTarget target1 = client.target(FULL_PATH);
		String response1 = target1.request().get(String.class);
		System.out.println(response1);
	}

}
